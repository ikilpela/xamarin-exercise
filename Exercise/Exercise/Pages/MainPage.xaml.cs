﻿using Exercise.Viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Exercise
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            submitBtn.Clicked += SubmitBtn_Clicked;
        }

        private void SubmitBtn_Clicked(object sender, EventArgs e)
        {
            
            var viewmodel = BindingContext as MainPageViewModel;

            // TIP: The text comes from property 'BioText' in ViewModels.MainPageViewModel class
            DisplayAlert("", viewmodel.BioText, "OK");
        }
    }
}
